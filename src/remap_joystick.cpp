#include <ros/ros.h>

#include <ros/package.h>
#include <nodelet/nodelet.h>

#include <fub_cargate_msgs/Activation.h>
#include <fub_cargate_msgs/NormalizedThrottleBrake.h>
#include <fub_cargate_msgs/NormalizedSteeringAngle.h>
#include <fub_cargate_msgs/Gear.h>
#include <sensor_msgs/Joy.h>
#include <nav_msgs/Odometry.h>




namespace fub_joystick {

class remap_joystick : public nodelet::Nodelet
{
public:
    remap_joystick() {}

    virtual ~remap_joystick() {}

	virtual void onInit() override
    {
        mSubscriberJoystic= getNodeHandle().subscribe("joy", 1, &remap_joystick::callbackJoy, this);
        mEngagedPublisher = getNodeHandle().advertise<fub_cargate_msgs::Activation>("command/activation", 10);
        mWantedNormThroBraPublisher = getNodeHandle().advertise<fub_cargate_msgs::NormalizedThrottleBrake>("command/normalized_throttle_brake", 10);
        mWantedNormSteerAnglePublisher = getNodeHandle().advertise<fub_cargate_msgs::NormalizedSteeringAngle>("command/normalized_steering_angle", 10);
        mWantedGearPublisher = getNodeHandle().advertise<fub_cargate_msgs::Gear>("command/gear", 10);
        mGear=8;
        fub_cargate_msgs::NormalizedThrottleBrake throbraMsg;
        throbraMsg.header.stamp = ros::Time::now();;
        throbraMsg.data = 0;
        mWantedNormThroBraPublisher.publish(throbraMsg);
	}


private:
    void callbackJoy(sensor_msgs::Joy const & joy){

        ros::Time now = ros::Time::now();
        fub_cargate_msgs::Gear gearMsg;
        gearMsg.header.stamp = now;

        fub_cargate_msgs::Activation activationMsg;
        activationMsg.header.stamp = now;
        activationMsg.data = 1;
        mEngagedPublisher.publish(activationMsg);


        mGear=mGear/(joy.buttons[4]+1);
        mGear=mGear*(joy.buttons[5]+1);
        if (mGear>8)
            mGear=8;
        if (mGear<1)
            mGear=1;
        gearMsg.data = mGear;
        mWantedGearPublisher.publish(gearMsg);


        fub_cargate_msgs::NormalizedThrottleBrake throbraMsg;
        throbraMsg.header.stamp = now;
        double speed=(joy.axes[2]+1.0)/2.0;
        double brake=(joy.axes[1]+1.0)/2.0;
        throbraMsg.data = speed-brake;
        mWantedNormThroBraPublisher.publish(throbraMsg);


        fub_cargate_msgs::NormalizedSteeringAngle steerMsg;
        steerMsg.header.stamp = now;
        steerMsg.data = joy.axes[0];
        mWantedNormSteerAnglePublisher.publish(steerMsg);

    }
    /// subscriber
    ros::Subscriber mSubscriberJoystic;

    /// publisher
    ros::Publisher mWantedNormThroBraPublisher;
    ros::Publisher mWantedNormSteerAnglePublisher;
    ros::Publisher mWantedGearPublisher;
    ros::Publisher mEngagedPublisher;


    double mGear;

};

}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(fub_joystick::remap_joystick, nodelet::Nodelet);
